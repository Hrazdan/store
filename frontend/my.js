var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('mydb');
var fs = require('fs');

$(document).ready(function(){
  db.serialize(function() {
    //db.run("CREATE TABLE mytable (info TEXT)");
  });
});
function Dump(){
  var com = db.each("SELECT * FROM sqlite_master",function(err, r){
   console.log(r);
  });
}
function Set(){
  var stmt = db.prepare("INSERT INTO mytable VALUES (?)");
  for (var i = 0; i < 10; i++) {
      stmt.run("Value " + i*10);
  }
  stmt.finalize();
}

function Get(){
 db.each("SELECT rowid AS id, info FROM mytable", function(err, row) {
      fs.appendFile("myfile.txt", row.info + "\n", function(err) {
    	   if(err) {
        	return console.log(err);
           }
           console.log("The file was saved!");
      }); 
  });
}

from pysimplesoap.client import SoapClient
import os, json
from datetime import datetime

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
FILENAME = BASE_DIR + "/rates.txt"

def GetRates():
    "Get exchange rates from cba and do filtering "
    rates = {}
    ISO = ['USD', 'RUB', 'EUR']
    wsdlPath =  "file:" + BASE_DIR + "/exchangerates.wsdl"
    client = SoapClient(wsdl=wsdlPath)
    retval = []
    retval.append(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    try:
        for i in ISO:
            response = client.ExchangeRatesLatestByISO(i)
            result = response['ExchangeRatesLatestByISOResult']
            result = result["Rates"]
            result = result[0]
            result = result['ExchangeRate']
            del result['Amount']

            if (float(result['Difference']) < 0):
                result['Difference'] = str(-1 * float(result['Difference']))
                result['Sign'] = "Minus"
            else:
                result['Difference'] = str(float(result['Difference']))
                result['Sign'] = "Plus"
            retval.append(result)
    except:
        retval = None
    return json.dumps(retval)



js = GetRates()
file = open(FILENAME, 'w')
file.write(js)
file.close()

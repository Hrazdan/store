from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models

class StoreUserManager(BaseUserManager):

    def create_user(self, username, first_name, password=None):
        if not username:
            raise ValueError('Username')
        if not first_name:
            raise ValueError('First name')

        user = self.model(
            username=username,
            first_name=first_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,  username, first_name, password):
        user = self.create_user(username=username,
                                first_name=first_name,
                                password=password
                                )

        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user

class StoreUser(AbstractUser):

    phone = models.CharField('Phone', max_length=15, blank=True, default="")
    address = models.CharField('Address', max_length=80, blank=True, default="")

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name',]
    objects = StoreUserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
from django.conf.urls import include, url

urlpatterns = [
    url(r'^$', 'Login.views.Start'),
    url(r'^login/$', 'Login.views.Login'),
    url(r'^logout/$', 'Login.views.Logout'),
]
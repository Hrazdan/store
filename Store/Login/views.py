from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.template.context_processors import csrf

from django.http.response import HttpResponse


from Login.ExchangeRates import GetRates

# Create your views here.
def Start(request):
    r = GetRates()
    context = {}
    context.update(csrf(request))
    context['rates'] = r
    return render_to_response('start.html',context)


def Login(request):
    context = {}
    if request.POST:
        uname = request.POST.get("username","")
        pswd = request.POST.get("password","")
        user = auth.authenticate(username = uname,password = pswd)
        context["login_error"]= False
        if user is not None:
            auth.login(request,user)
            return redirect("/mainpage")
        else:
            context["login_error"] = True
            r = GetRates()
            context.update(csrf(request))
            context['rates'] = r
            return render_to_response('start.html',context)
    else:
        context["login_error"] = True
        r = GetRates()
        context.update(csrf(request))
        context['rates'] = r
        return render_to_response('start.html',context)


def Logout(request):
    auth.logout(request)
    return redirect('/')
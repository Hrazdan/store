import os
import json

FILE = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/Rates/rates.txt"


def GetRates():
	try:
		file = open(FILE, 'r')
		a = json.load(file)
		a.pop(0)
		file.close()
		return a
	except:
		return [{u'Rate': u'000.0000', u'Difference': u'0.00', u'ISO': u'USD', u'Sign': u'Plus'}, {u'Rate': u'0.0000', u'Difference': u'0.00', u'ISO': u'RUB', u'Sign': u'Minus'}, {u'Rate': u'000.0000', u'Difference': u'0.00', u'ISO': u'EUR', u'Sign': u'Plus'}]
	#content = file.read()
	


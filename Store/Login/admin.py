from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from .forms import UserChangeForm
from .forms import UserCreationForm
from .models import StoreUser


class UserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = [
        'username',
        'email',
        'first_name',
        'is_superuser',
        'last_name',
    ]

    list_filter = ('is_superuser',)

    fieldsets = (
                (None, {'fields': ('username', 'password')}),
                ('Personal info', {
                 'fields': (
                     'first_name',
                     'last_name',
                     'email',
                     'phone',
                     'address',
                 )}),
                ('Permissions', {'fields': ('is_superuser',)}),
                ('Important dates', {'fields': ('last_login',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'first_name',
                'password1',
                'password2',
            )}
         ),
    )

    search_fields = ('username',)
    ordering = ('username',)
    filter_horizontal = ()


admin.site.register(StoreUser, UserAdmin)
admin.site.unregister(Group)
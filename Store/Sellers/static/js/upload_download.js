var fs = require('fs');

 function progressHandlingFunction(e) {
        if (e.lengthComputable) {
             $('progress').attr({value: e.loaded, max: e.total});
        }
 }

 function completeHandler(e) {
     console.log(e);
 }

 function errorHandler(e) {
     console.log(e);
 }

 function download(){
     console.log("down");
     $.ajax({
            url: "/file/downloaddb/", // the endpoint
            type: "POST", // http method
            success: function (response) {
                console.log(response);
                fs.writeFile("./sqliteDB", response, function(err) {
                    if(err) {
                        alert("error");
                    }
                });
                fs.close();

            }

        });
 }

var csrftoken = $.cookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
};

 $('document').ready(function() {
     //Setup ajax

     $.ajaxSetup({
         beforeSend: function (xhr, settings) {
             if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                 // Send the token to same-origin, relative URLs only.
                 // Send the token only if the method warrants CSRF protection
                 // Using the CSRFToken value acquired earlier
                 xhr.setRequestHeader("X-CSRFToken", csrftoken);
             }
         }
     });

     //Setup handlers
     $('#upload').click(function () {
         var formData = new FormData($('form')[0]);
         $.ajax({
             url: '/file/uploaddb/',  //Server script to process data
             type: 'POST',
             xhr: function () {  // Custom XMLHttpRequest
                 var myXhr = $.ajaxSettings.xhr();
                 if (myXhr.upload) { // Check if upload property exists
                     myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                 }
                 return myXhr;
             },
             //Ajax events
             //beforeSend: beforeSendHandler,
             success: completeHandler,
             error: errorHandler,
             // Form data
             data: formData,
             //Options to tell jQuery not to process data or worry about content-type.
             cache: false,
             contentType: false,
             processData: false
         });
     });

     $("#download").click(download);

 });


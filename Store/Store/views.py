from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.http.response import HttpResponse

from django.contrib.auth import get_user_model

from django.contrib.auth.models import User
from django.template.context_processors import csrf


def mainpage(request):
    uname = auth.get_user(request).username
    user = get_user_model().objects.get(username=uname)
    c = {}
    c.update(csrf(request))
    if not user.is_superuser:#if user is seller redirect to seller`s mainpage
        c['username'] = uname
        return render_to_response('sellers_mainpage.html',c)
    else:#if admin redirect to main page
        c['username'] = uname
        return render_to_response('mainpage.html',c)

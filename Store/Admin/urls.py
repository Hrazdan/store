from django.conf.urls import include, url

urlpatterns = [
    url(r'^$', 'Admin.views.administrate'),
    url(r'^sellers/$', 'Admin.views.getSellers'),
    url(r'^logout/$', 'Login.views.Logout'),
    url(r'^addorupdate/$','Admin.views.addOrUpdateSellers'),
    url(r'^deleteseller/$','Admin.views.deleteSeller'),
   # url(r'^providers/$','Admin.views.getProviders'),
    url(r'^debtors/$','Admin.views.getDebtor'),
    url(r'^providers/$','Admin.views.getProvider'),


    # url for testing
    url(r'^test/$','Admin.views.test'),
    url(r'^adddebtors/$','Admin.views.addOrUpdateDebtors'),
    url(r'^deldebtors/$','Admin.views.deleteDebtors'),
    url(r'^addproviders/$','Admin.views.addOrUpdateProviders'),
    url(r'^delproviders/$','Admin.views.deleteProviders'),
]

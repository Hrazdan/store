from django.db import models
from django.db.models import Sum, F
import datetime

# Create your models here.

class Type(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        db_table = "Type"


class Producer(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        db_table = "Producer"


class Measure(models.Model):
    name = models.CharField(max_length=10)

    class Meta:
        db_table = "Measure"


class Provider(models.Model):
    f_name = models.CharField(max_length=20)
    l_name = models.CharField(max_length=30, blank=True, default="")
    phone = models.CharField(max_length=15, blank=True, default="")
    email = models.EmailField(null=True, blank=True, default="")
    debtProvider = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    note = models.TextField(blank=True, default="")

    class Meta:
        db_table = "Provider"


class ProviderChronogy(models.Model):
    paymentDate = models.DateField()
    paid = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    lotId = models.ForeignKey("Lot")
    note = models.TextField(default="", blank=True)

    class Meta:
        db_table = "ProviderChronology"


class Product(models.Model):
    name = models.CharField(max_length = 20)
    typeId = models.ForeignKey(Type)
    measureId = models.ForeignKey(Measure)
    producerId = models.ForeignKey(Producer)
    sellPrice = models.DecimalField(decimal_places=2, max_digits=10)
    barCode = models.CharField(max_length=20)
    quantity = models.DecimalField(decimal_places=4, max_digits=10, default=0)

    class Meta:
        db_table = "Product"

    def __unicode__(self):
        return self.name


class Lot(models.Model):
    providerId = models.ForeignKey(Provider)
    receiveDate = models.DateField()
    products = models.ManyToManyField(Product, through="LotProduct")
    total = models.DecimalField(max_digits=10, decimal_places=2)
    paid = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "Lot"


class LotProduct(models.Model):
    lotId = models.ForeignKey(Lot)
    productId = models.ForeignKey(Product)
    costPrice = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.DecimalField(max_digits=10, decimal_places=2)
    expDate = models.DateField()

    class Meta:
        db_table = "LotProduct"



class DebtorChronology(models.Model):
    paymentDate = models.DateField()
    paid = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    debtId = models.ForeignKey("Debt")
    note = models.TextField(default="", blank=True)

    class Meta:
        db_table = "DebtorChronology"


class Debtor(models.Model):
    f_name = models.CharField(max_length=20)
    l_name = models.CharField(max_length=30, blank=True, default="")
    phone = models.CharField(max_length=15, blank=True, default="")
    email = models.EmailField(null=True, blank=True, default="")
    debtDebtor = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    note = models.TextField(blank=True, default="")

    class Meta:
        db_table = "Debtor"


class Transaction(models.Model):
    debtTransaction = models.BooleanField()
    sellDate = models.DateField()
    total = models.DecimalField(max_digits=10, decimal_places=2)
    products = models.ManyToManyField(Product, through="Sell")

    class Meta:
        db_table = "Transaction"


class Debt(models.Model):
    transactionId = models.ForeignKey(Transaction)
    debtorId = models.ForeignKey(Debtor)
    expPayment = models.DateField()
    paid = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "Debt"



class Sell(models.Model):
    productId = models.ForeignKey(Product)
    transactionId = models.ForeignKey(Transaction)
    quantity = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        db_table = "Sell"



class Selections:

    @staticmethod
    def getSalesByTypename(typeName, startDate, endDate):

        products = Product.objects.filter(typeId__name__exact=typeName,
                                          transaction__sellDate__range=(startDate, endDate)).\
            annotate(total=Sum('sell__quantity')).order_by('-total').values("name", "total")

        return products


    @staticmethod
    def productSalesQuantity(productName, startDate, endDate):

        totalSell = Product.objects.filter(name=productName, sell__transactionId__sellDate__range=(startDate, endDate)).\
            aggregate(quantity=Sum('sell__quantity'))
        
        total = float(totalSell['quantity'])
        return total


    @staticmethod
    def lotsByDateRange(startDate,endDate):
        lotProducts = []
        for lot in Lot.objects.filter(receiveDate__range=(startDate,endDate)).order_by('receiveDate'):
            tempLot = {}
            tempLot['date'] = lot.receiveDate

            products = []
            for product in lot.lotproduct_set.all():
                temp ={}
                temp['name'] = product.productId.name
                temp['costPrice'] = product.costPrice
                temp['quantity'] = product.quantity
                temp['expDate']  = product.expDate
                products.append(temp)
            tempLot['products'] = products

            lotProducts.append(tempLot)

        return lotProducts

    
    @staticmethod
    def productRemainderByProvider(providerId):
        lots = Lot.objects.filter(providerId=providerId)
        products = Product.objects.filter(lot__in=lots).distinct().values('name', 'quantity')
        return products

    @staticmethod
    def lotsWithDebtByProvider(providerId):
        lots = Lot.objects.annotate(dif=F('total')-F('paid')).filter(dif__gt=0)
        return lots


    @staticmethod
    def lotPayments(lotId):
        payments = ProviderChronology.objects.filter(lotid=lotId).values('date', 'paid','note')
        return payments


    @staticmethod
    def debtsForDebtor(debtorId):
        debts = Debt.objects.filter(debtorId=debtorId)
        return debts


    @staticmethod
    def debtPayments(debtId):
        payments = DebtorChronology.objects.filter(debtId=debtId).values('date', 'paid','note')
        return payments

    @staticmethod
    def debtorsDepts(debtorId=None):
        debtors = Debtor.objects.annotate(total=Sum(F('debt__transactionId__total')-F('debt__paid')))
        if not debtorId is None:
            debtors = debtors.filter(id=debtorId)
        return debtors


    @staticmethod
    def debtorsExpiredDepts(debtorId=None):
        debtors = Debtor.objects.filter(debt__expPayment__lt=datetime.date.today())
        debtors = debtors.annotate(total=Sum(F('debt__transactionId__total')-F('debt__paid')))
        if not debtorId is None:
            debtors = debtors.filter(id=debtorId)
        return debtors
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import render_to_response, redirect
from django.contrib import auth
from django.http.response import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from Login.models import StoreUser
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import get_user_model
from Login.views import Logout
import json
from .models import *
import datetime

# Create your views here.


@login_required
def administrate(request):
    context = {}
    uname = auth.get_user(request).username
    user = get_user_model().objects.get(username=uname)
    if user.is_superuser:
        context['username'] = uname
        return render_to_response("admin_mainpage.html",context)

    return redirect('/mainpage/')

@login_required
def getSellers(request):
    context = {}
    uname = auth.get_user(request).username
    user = get_user_model().objects.get(username=uname)
    if user.is_superuser:
        # all user which is not superuser
        allSellers = get_user_model().objects.all().filter(is_superuser=False, is_active=True)
        for seller in allSellers:
            seller.password = ""
        context['username'] = uname
        context['sellers'] = allSellers
        return render_to_response('seller.html',context)
    else:
       return redirect('/mainpage/')

def Check(dicts):
    keys=["id", "username", "password", "first_name", "last_name", "email", "phone","address"]
    for dict in dicts:
        for i in keys:
            try:
                dict[i]
                if i=="id" and dict[i]=="" and dict["password"]=="":
                    return False
            except:
                return False
        if dict["username"]=="" or dict["first_name"]=="":
            return False
        if not(dbCheck(dict)):
            return False
    return True

def dbCheck(dict):
    if dict["id"] == "":
        try:
            get_user_model().objects.get(username=dict["username"])
            return False
        except:
            pass
    else:
        try:
            get_user_model().objects.get(id=int(dict["id"]))
            try:
                if int(dict["id"]) != get_user_model().objects.get(username=dict["username"]).id:
                    return False
            except:
                pass
        except:
            return False
    return True

@login_required
def addOrUpdateSellers(request):
    uname = auth.get_user(request).username
    user = get_user_model().objects.get(username=uname)
    if user.is_superuser:
        sellers = json.loads(request.POST.get("sellers", "[]"))
        if Check(sellers):
            for seller in sellers:
                if seller["id"] == "":
                    user = get_user_model().objects.create_user(username=seller["username"],
                                            first_name=seller["first_name"],
                                            password=seller["password"])
                    user.last_name = seller['last_name']
                    user.email = seller['email']
                    user.phone = seller['phone']
                    user.address = seller['address']
                    user.save()
                else:
                    user = get_user_model().objects.get(id=int(seller['id']))
                    user.username = seller["username"]
                    user.first_name = seller["first_name"]
                    user.last_name = seller["last_name"]
                    user.email = seller["email"]
                    user.phone = seller['phone']
                    user.address = seller['address']
                    if seller["password"] != "":
                        user.set_password(seller["password"])
                    user.save()
            return HttpResponse('OK')
        else:
            return HttpResponse("ERROR")
    else:
         return HttpResponse("ERROR")


@login_required
def deleteSeller(request):
    uname = auth.get_user(request).username
    user = get_user_model().objects.get(username=uname)
    if user.is_superuser:
        try:
            id = int(request.POST.get('id', '')[1:-1])
            seller = get_user_model().objects.get(id=id)
            seller.is_active = False
            seller.save()
            return HttpResponse('OK')
        except:
            return HttpResponse('ERROR')
    else:
        return HttpResponse('ERROR')




@login_required
def getDebtor(request):
    context = {}
    allDebtor = Debtor.objects.all()
    context['debtors'] = allDebtor
    return render_to_response("debtor.html", context)



def CheckDebtors(dicts):
    keys=["id", "f_name", "l_name", "phone","email","note"]
    for dict in dicts:
        for i in keys:
            try:
                dict[i]
                if i=="id" and dict[i]=="" and dict["f_name"]=="":
                    return False
            except:
                return False
        if dict["f_name"]=="":
            return False
        if not(dbCheckDebtors(dict)):
            return False
    return True

def dbCheckDebtors(dict):
    if dict["id"] == "":
        try:
            Debtor.objects.get(f_name=dict["f_name"])
            return False
        except:
            pass
    else:
        try:
            Debtor.objects.get(id=int(dict["id"]))
            try:
                if int(dict["id"]) != Debtors.objects.get(f_name=dict["f_name"]).id:
                    return False
            except:
                pass
        except:
            return False
    return True


@login_required
def addOrUpdateDebtors(request):
        debtors = json.loads(request.POST.get("debtors", "[]"))
        if CheckDebtors(debtors):
            for debtor in debtors:
                if debtor["id"] == "":
                    Debtor.objects.create(f_name=debtor["f_name"],
                    l_name = debtor['l_name'],
                    phone = debtor['phone'],
                    email = debtor['email'],
                    note = debtor['note'])
                else:
                    Debtor.objects.filter(id=int(debtor['id'])).update(
                    f_name = debtor["f_name"],
                    l_name = debtor["l_name"],
                    phone = debtor['phone'],
                    email = debtor['email'],
                    note = debtor['note'] )
            return HttpResponse('OK')
        else:
            return HttpResponse("ERROR")


@login_required
def deleteDebtors(request):
    id = int(request.POST.get('id', '')[1:-1])
    debtor = Debtor.objects.get(id=id)
    debtor.delete()
    return HttpResponse('OKkkkkkk')




@login_required
def getProvider(request):
    context = {}
    allProvider = Provider.objects.all()
    context['allProviders'] = allProvider
    return render_to_response("provider.html", context)



def CheckProviders(dicts):
    keys=["id", "f_name", "l_name", "phone","email","note"]
    for dict in dicts:
        for i in keys:
            try:
                dict[i]
                if i=="id" and dict[i]=="" and dict["f_name"]=="":
                    return False
            except:
                return False
        if dict["f_name"]=="":
            return False
        if not(dbCheckProviders(dict)):
            return False
    return True

def dbCheckProviders(dict):
    if dict["id"] == "":
        try:
            Provider.objects.get(f_name=dict["f_name"])
            return False
        except:
            pass
    else:
        try:
            Provider.objects.get(id=int(dict["id"]))
            try:
                if int(dict["id"]) != Provider.objects.get(f_name=dict["f_name"]).id:
                    return False
            except:
                pass
        except:
            return False
    return True


@login_required
def addOrUpdateProviders(request):
        providers = json.loads(request.POST.get("debtors", "[]")) 
        if CheckProviders(fproviders):
            for provider in fproviders:
                if provider["id"] == "":
                    Provider.objects.create(f_name=provider["f_name"],
                    l_name = provider['l_name'],
                    phone = provider['phone'],
                    email = provider['email'],
                    note = provider['note'])
                else:
                    Provider.objects.filter(id=int(provider['id'])).update(
                    f_name = provider["f_name"],
                    l_name = provider["l_name"],
                    phone = provider['phone'],
                    email = provider['email'],
                    note = provider['note'] )
            return HttpResponse('OK')
        else:
            return HttpResponse("ERROR")


@login_required
def deleteProviders(request):
    id = int(request.POST.get('id', '')[1:-1])
    provider = Provider.objects.get(id=id)
    provider.delete()
    return HttpResponse('OKkkkkkk')

# view for test
def test(request):
    date1 = datetime.date(2015,8,15)
    date2 = datetime.date(2015,8,25)
    context = {}

    context['products'] = Selections.productRemainderByProvider(1)
    return  render_to_response('test.html', context)


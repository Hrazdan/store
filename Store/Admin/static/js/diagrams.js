window.onload = function () {

    var chart = new CanvasJS.Chart("chartContainer",
        {
            title: {
                text: "Gaming Consoles Sold in 2012"
            },
            legend: {
                maxWidth: 500,
                itemWidth: 120
            },
            data: [
                {
                    type: "pie",
                    showInLegend: true,
                    legendText: "{indexLabel}",
                    dataPoints: [
                        {y: 4181563, indexLabel: "PlayStation 3"},
                        {y: 2175498, indexLabel: "Wii"},
                        {y: 3125844, indexLabel: "Xbox 360"},
                        {y: 1176121, indexLabel: "Nintendo DS"},
                        {y: 1727161, indexLabel: "PSP"},
                        {y: 4303364, indexLabel: "Nintendo 3DS"},
                        {y: 1717786, indexLabel: "PS Vita"},
                        {y: 9712156, indexLabel: "Valod"}
                    ]
                }
            ]
        });
    chart.render();
}



window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Different Marker Types"
      },

      data: [
      {
        type: "line",
        showInLegend: "true",
        markerType: "circle",
        legendText: "circle",
        dataPoints: [
        { x: 10, y: 61 },
        { x: 20, y: 59 },
        { x: 30, y: 50},
        { x: 40, y: 65},
        { x: 50, y: 95},
        { x: 60, y: 68},
        { x: 70, y: 58},
        { x: 80, y: 44},
        { x: 90, y: 56}

        ]
      },
      {
        type: "line",
        showInLegend: "true",
        markerType: "triangle",
        legendText: "triangle",
        dataPoints: [
        { x: 10, y: 31 },
        { x: 20, y: 35},
        { x: 30, y: 40 },
        { x: 40, y: 45 },
        { x: 50, y: 45 },
        { x: 60, y: 48 },
        { x: 70, y: 43 },
        { x: 80, y: 41 },
        { x: 90, y: 44}

        ]
      },
      {
        type: "line",
        showInLegend: "true",
        markerType: "square",
        legendText: "square",
        dataPoints: [
        { x: 10, y: 45 },
        { x: 20, y: 50},
        { x: 30, y: 30 },
        { x: 40, y: 35 },
        { x: 50, y: 35 },
        { x: 60, y: 38},
        { x: 70, y: 38 },
        { x: 80, y: 34 },
        { x: 90, y: 28}

        ]
      },
      {
        type: "line",
        showInLegend: "true",
        legendText: "cross",
        markerType: "cross",
        dataPoints: [
        { x: 10, y: 71 },
        { x: 20, y: 55},
        { x: 30, y: 20 },
        { x: 40, y: 25 },
        { x: 50, y: 27 },
        { x: 60, y: 28 },
        { x: 70, y: 28 },
        { x: 80, y: 24 },
        { x: 90, y: 14}

        ]
      },
         {
        type: "line",
        showInLegend: "true",
        legendText: "no marker",
        markerType: "none",
        dataPoints: [
        { x: 10, y: 91 },
        { x: 20, y: 85},
        { x: 30, y: 80 },
        { x: 40, y: 87 },
        { x: 50, y: 77 },
        { x: 60, y: 78 },
        { x: 70, y: 88 },
        { x: 80, y: 74 },
        { x: 90, y: 84}

        ]
      }
      ]
    });

chart.render();
}

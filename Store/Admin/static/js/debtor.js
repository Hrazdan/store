var DrowNumber=null;
var debtors_global=[];
var Dglobal_data=[];
var DcurCell=null;

/*Get active input
*/
function DebtorcurrentCell(cell1){
    DcurCell=cell1;
    Dmynum=DcurCell.id.slice(4);
    DrowNumber=~~(Dmynum/10);
}
/*Handler to catch any change of any input
*/
function debtorChange(el1){
    el1.style.backgroundColor="#ffbee2";
    var Dinputs=$('#'+DrowNumber+' input');
    var DcolumnsName=['id','f_name','l_name','address','phone',];

    var DtableColumns={};
    for(var i=0;i<DcolumnsName.length;i++){
        DtableColumns[DcolumnsName[i]]=$(Dinputs[i]).val();
    }
    debtors_global[DrowNumber]=DtableColumns;

    if(DcheckErrors().length==0){
        $('#tooltip').css({"background-color":"transparent"});
        $('#Dsend').prop('disabled',false);
    }
    else {
        $('#tooltip').css({"background-color":"red"});
        $('#Dsend').prop('disabled',true);
    }
}
/*check errors
*/
function DcheckErrors(){
    var error=0;
    var Derrors=[];
    var Dinput=$('input');
    var isEmpty=false;
    for(var i=0;i<Dinput.length;i++){
        var DcCell=(Dinput[i]);
        if(DcCell.id.slice(-1)==0){
           if($(DcCell).val()==''){
              isEmpty=true;
           }
        }
        if(isEmpty==true) {
            if (DcCell.id.slice(-1) == 1 ) {
                if ($(DcCell).val() == '') {
                    $(DcCell).addClass('error').removeClass('correct');
                    $(DcCell).attr('placeholder','Լրացրեք դաշտը');
                    Derrors.push(DcCell.id.slice(-1));
                    error = 1;
                } else {
                    $(DcCell).addClass('correct').removeClass('error');
                    $(DcCell).removeAttr('placeholder');
                }
            }
        }

        if(DcCell.id.slice(-1)==4){
            if($(DcCell).val() != ''){
                var telpatt= /^[0-9]*$/i;
                if(telpatt.test($(DcCell).val()) && $(DcCell).val().length>5 && $(DcCell).val().length<10) {
                    $(DcCell).addClass('correct').removeClass('error');
                    $(DcCell).removeAttr('data-tooltip');
                } else {
                    $(DcCell).addClass('error').removeClass('correct');
                    $(DcCell).attr('data-tooltip','Ոչ ճիշտ հեռ․համար');
                    errorMessage();
                    Derrors.push(DcCell.id.slice(-1));
                    error=3;
                }
            } else {
                $(DcCell).addClass('correct').removeClass('error');
                $(DcCell).removeAttr('data-tooltip');
            }
        }
    }
    var Dresult = Derrors,
    j = Dresult.length;
    Dresult.sort();
    while (j--) {
        if (Dresult[j] == Dresult[j-1]) {
            Dresult.splice(j, 1);
        }
    }
    return Dresult;
}
function errorMessage(){
    $("[data-tooltip]").mousemove(function (eventObject) {
        $data_tooltip = $(this).attr("data-tooltip");
        $("#tooltip").text($data_tooltip)
                     .css({
                         "top" : eventObject.pageY + 5,
                        "left" : eventObject.pageX + 5
                     })
                     .show();
    }).mouseout(function () {
        $("#tooltip").text("")
                     .css({
                         "top" : 0,
                        "left" : 0
                     }).hide();
    });
}
/*forrmatter of collected data(to send to server)
*/
function debtorVals() {
    Dglobal_data.length=0;
    for(var i=1;i<debtors_global.length;i++){
        if(debtors_global[i]!=undefined){
            Dglobal_data.push(debtors_global[i]);
        } else{
            continue;
        }
    }
    debtors_global.length=0;
}
/*add new row
*/
function debtoraddRow(){
    $('#Dsend').prop('disabled',true);
    var Dtbody=$('#debtor-tbody');
    var DrowId=Dtbody.children().length+1;
    var Dtr=$('<tr>');
    Dtr.attr('id',DrowId);
    Dtr.attr('onchange',"debtorChange(this)");
    Dtbody.append(Dtr);

    for(var i=0;i<5;i++){
        var Dtd=$('<td>');
        var Dinput=$('<input>');
        Dinput.attr('id','txtd'+DrowId+''+i);
        Dinput.attr('type','text');
        Dinput.attr('class','correct');
        Dinput.attr('onfocus','DebtorcurrentCell(this)');
        Dtd.append(Dinput);
        Dtr.append(Dtd);
    }
    $('#txtd'+DrowId+''+0).attr('readonly','true');
    $('#txtd'+DrowId+''+4).attr('type','tel');
}

/*function gets all tokens
*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
/*Store all tokens to csrftoken global variable
*/
var csrftoken = $.cookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
};

$(document).ready(function(){
    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
});

/*Function sends ajax request which contains all changes and updates
*/
function Debtorsend(){
    if(DcheckErrors().length==0) {
        debtorVals();
        $.ajax({
            url: "/administrate/addorupdate/", // the endpoint
            type: "POST", // http method
            data: {"debtors": JSON.stringify(Dglobal_data)},
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                } else {

                    console.log(JSON.stringify(Dglobal_data));
                }
            }

        });
        $('#Dsend').prop('disabled',true);
    }
}
/*ajax request to delete seller
*/
function debtordeleteAjax(){
    DrowId=$('#txtd'+DrowNumber+0).val();
    if(DrowId!=undefined) {
        $.ajax({
            url: "/administrate/deletedebtor/",
            type:"POST",
            data: {"id": JSON.stringify(DrowId)},
            success: function () {
                //here you do the processing needed to remove the row from the html;
            }
        });
    }
}
$(document).ready(function() {
    $('#debtor-table').DataTable( {
        "pagingType": "full_numbers",
        "info":false,
        "lengthMenu": [ [10, 15, 20, -1], [10, 15, 20, "Բոլորը"] ],
        "searching": true,
        "language": {
            "lengthMenu": "Տեսնել _MENU_ տող",
            "paginate": {
                "first": "Սկիզբ",
                "previous": "Նախորդ",
                "next": "Հաջորդ",
                "last": "Վերջ"
            }
        }

    } );
} );



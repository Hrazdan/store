var ProwNumber=null;
var provider_global=[];
var Pglobal_data=[];
var PcurCell=null;

/*Get active input
*/
function currentCellProvider(Pcell){
    PcurCell=Pcell;
    Pmynum=PcurCell.id.slice(4);
    ProwNumber=~~(Pmynum/10);
}
/*Handler to catch any change of any input
*/
function providerChange(Pel){
    Pel.style.backgroundColor="#ffbee2";
    var Pinputs=$('#'+ProwNumber+' input');
    var PcolumnsName=['id','f_name','l_name','phone','email','note'];

    var PtableColumns={};
    for(var i=0;i<PcolumnsName.length;i++){
        PtableColumns[PcolumnsName[i]]=$(Pinputs[i]).val();
    }
    provider_global[ProwNumber]=PtableColumns;

    if(ProvidcheckErrors().length==0){
        $('#tooltip').css({"background-color":"transparent"});
        $('#Psend').prop('disabled',false);
    }
    else {
        $('#tooltip').css({"background-color":"red"});
        $('#Psend').prop('disabled',true);
    }
}
/*check errors
*/
function ProvidcheckErrors(){
    var error=0;
    var Perrors=[];
    var Pinput=$('input');
    var PisEmpty=false;
    for(var i=0;i<Pinput.length;i++){
        var PcCell=(Pinput[i]);
        if(PcCell.id.slice(-1)==0){
           if($(PcCell).val()==''){
              PisEmpty=true;
           }
        }
        if (PcCell.id.slice(-1) == 1) {
            if ($(PcCell).val().length == 0) {
                $(PcCell).addClass('error').removeClass('correct');
                $(PcCell).attr('placeholder','Լրացրեք դաշտը');
                Perrors.push(PcCell.id.slice(-1));
                error = 1;
            } else {
                $(PcCell).addClass('correct').removeClass('error');
                $(PcCell).removeAttr('placeholder');
            }
        }
        if(PcCell.id.slice(-1)==4){
            if($(PcCell).val() != '') {
                var Ppattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(Ppattern.test($(PcCell).val())){
                    $(PcCell).addClass('correct').removeClass('error');
                    $(PcCell).removeAttr('data-tooltip');
                } else {
                    $(PcCell).addClass('error').removeClass('correct');
                    $(PcCell).attr('data-tooltip','Ոչ ճիշտ էլ․փոստ');
                    errorMessage();
                    Perrors.push(PcCell.id.slice(-1));
                    error=2;
                }
            } else {
                $(PcCell).addClass('correct').removeClass('error');
                $(PcCell).removeAttr('data-tooltip');

            }
        }
        if(PcCell.id.slice(-1)==3){
            if($(PcCell).val() != ''){
                var telpatt= /^[0-9]*$/i;
                if(telpatt.test($(PcCell).val()) && $(PcCell).val().length>5 && $(PcCell).val().length<10) {
                    $(PcCell).addClass('correct').removeClass('error');
                    $(PcCell).removeAttr('data-tooltip');

                } else {
                    $(PcCell).addClass('error').removeClass('correct');
                    $(PcCell).attr('data-tooltip','Ոչ ճիշտ հեռ․համար');
                    errorMessage();
                    Perrors.push(PcCell.id.slice(-1));
                    error=3;
                }
            } else {
                $(PcCell).addClass('correct').removeClass('error');
                $(PcCell).removeAttr('data-tooltip');
            }
        }
    }
    var Presult = Perrors,
    j = Presult.length;
    Presult.sort();
    while (j--) {
        if (Presult[j] == Presult[j-1]) {
            Presult.splice(j, 1);
        }
    }
    return Presult;
}

function errorMessage(){
    $("[data-tooltip]").mousemove(function (eventObject) {
        $data_tooltip = $(this).attr("data-tooltip");
        $("#tooltip").text($data_tooltip)
                     .css({
                         "top" : eventObject.pageY + 5,
                        "left" : eventObject.pageX + 5
                     })
                     .show();
    }).mouseout(function () {
        $("#tooltip").text("")
                     .css({
                         "top" : 0,
                        "left" : 0
                     }).hide();
    });
}
/*formatter of collected data(to send to server)
*/
function ProviderVals() {
    Pglobal_data.length=0;
    for(var i=1;i<provider_global.length;i++){
        if(provider_global[i]!=undefined){
            Pglobal_data.push(provider_global[i]);
        } else{
            continue;
        }
    }
    provider_global.length=0;
}
/*add new row
*/
function ProviderAddRow(){
    $('#Psend').prop('disabled',true);
    var Ptbody=$('#provider-tbody');
    var ProwId=Ptbody.children().length+1;
    var Ptr=$('<tr>');
    Ptr.attr('id',ProwId);
    Ptr.attr('onchange',"providerChange(this)");
    Ptbody.append(Ptr);

    for(var i=0;i<6;i++){
        var Ptd=$('<td>');
        var Pinput=$('<input>');
        Pinput.attr('id','Ptxt'+ProwId+''+i);
        Pinput.attr('type','text');
        Pinput.attr('class','correct');
        Pinput.attr('onfocus','currentCellProvider(this)');
        Ptd.append(Pinput);
        Ptr.append(Ptd);
    }
    $('#Ptxt'+ProwId+''+0).attr('readonly','true');
    $('#Ptxt'+ProwId+''+4).attr('type','email');
    $('#Ptxt'+ProwId+''+3).attr('type','tel');
}

/*function gets all tokens
*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
/*Store all tokens to csrftoken global variable
*/
var csrftoken = $.cookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
};

$(document).ready(function(){
    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
});

/*Function sends ajax request which contains all changes and updates
*/
function SendProviderRow(){
    if(ProvidcheckErrors().length==0) {
        ProviderVals();
        $.ajax({
            url: "/administrate/addorupdate/", // the endpoint
            type: "POST", // http method
            data: {"sellers": JSON.stringify(Pglobal_data)},
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                } else {

                    console.log(JSON.stringify(Pglobal_data));
                }
            }

        });
        $('#Psend').prop('disabled',true);
    }
}
/*ajax request to delete seller
*/
function ProviddeleteAjax(){
    ProwId=$('#Ptxt'+ProwNumber+0).val();
    if(ProwId!=undefined) {
        $.ajax({
            url: "/administrate/deleteProvider/",
            type:"POST",
            data: {"id": JSON.stringify(ProwId)},
            success: function (response) {
                //here you do the processing needed to remove the row from the html;
            }
        });
    }
}
$(document).ready(function() {
    $('#provider-table').DataTable( {
        "pagingType": "full_numbers",
        "info":false,
        "lengthMenu": [ [10, 15, 20, -1], [10, 15, 20, "Բոլորը"] ],
        "searching": true,
        "language": {
            "lengthMenu": "Տեսնել _MENU_ տող",
            "paginate": {
                "first": "Սկիզբ",
                "previous": "Նախորդ",
                "next": "Հաջորդ",
                "last": "Վերջ"
            }
        }

    } );
} );

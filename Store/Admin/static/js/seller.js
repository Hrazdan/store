var rowNumber=null;
var sellers_global=[];
var global_data=[];
var curCell=null;

/*Get active input
*/
function currentCell(cell){
    curCell=cell;
    mynum=curCell.id.slice(3);
    rowNumber=~~(mynum/10);
}
/*Handler to catch any change of any input
*/
function sellerChange(el){
    el.style.backgroundColor="#ffbee2";
    var inputs=$('#'+rowNumber+' input');
    var columnsName=['id','username','password','first_name','last_name','email','phone','address'];

    var tableColumns={};
    for(var i=0;i<columnsName.length;i++){
        tableColumns[columnsName[i]]=$(inputs[i]).val();
    }
    sellers_global[rowNumber]=tableColumns;

    if(checkErrors().length==0){
        $('#tooltip').css({"background-color":"transparent"});
        $('#send').prop('disabled',false);
    }
    else {
        $('#tooltip').css({"background-color":"red"});
        $('#send').prop('disabled',true);
    }
}
/*check errors
*/
function checkErrors(){
    var error=0;
    var errors=[];
    var input=$('input');
    var isEmpty=false;
    for(var i=0;i<input.length;i++){
        var cCell=(input[i]);
        if(cCell.id.slice(-1)==0){
           if($(cCell).val()==''){
              isEmpty=true;
           }
        }
        if(isEmpty==true) {
            if (cCell.id.slice(-1) == 2 ) {
                if ($(cCell).val() == '') {
                    $(cCell).addClass('error').removeClass('correct');
                    $(cCell).attr('placeholder','Լրացրեք դաշտը');
                    errors.push(cCell.id.slice(-1));
                    error = 1;
                } else {
                    $(cCell).addClass('correct').removeClass('error');
                    $(cCell).removeAttr('placeholder');
                }
            }
        }
        if (cCell.id.slice(-1) == 1 || cCell.id.slice(-1) == 3) {
            if ($(cCell).val().length == 0) {
                $(cCell).addClass('error').removeClass('correct');
                $(cCell).attr('placeholder','Լրացրեք դաշտը');
                errors.push(cCell.id.slice(-1));
                error = 1;
            } else {
                $(cCell).addClass('correct').removeClass('error');
                $(cCell).removeAttr('placeholder');
            }
        }
        if(cCell.id.slice(-1)==5){
            if($(cCell).val() != '') {
                var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
                if(pattern.test($(cCell).val())){
                    $(cCell).addClass('correct').removeClass('error');
                    $(cCell).removeAttr('data-tooltip');
                } else {
                    $(cCell).addClass('error').removeClass('correct');
                    $(cCell).attr('data-tooltip','Ոչ ճիշտ էլ․փոստ');
                    errorMessage();
                    errors.push(cCell.id.slice(-1));
                    error=2;
                }
            } else {
                $(cCell).addClass('correct').removeClass('error');
                $(cCell).removeAttr('data-tooltip');
            }
        }
        if(cCell.id.slice(-1)==6){
            if($(cCell).val() != ''){
                var telpatt= /^[0-9]*$/i;
                if(telpatt.test($(cCell).val()) && $(cCell).val().length>5 && $(cCell).val().length<10) {
                    $(cCell).addClass('correct').removeClass('error');
                    $(cCell).removeAttr('data-tooltip');

                } else {
                    $(cCell).addClass('error').removeClass('correct');
                    $(cCell).attr('data-tooltip','Ոչ ճիշտ հեռ․համար');
                    errorMessage();
                    errors.push(cCell.id.slice(-1));
                    error=3;
                }
            } else {
                $(cCell).addClass('correct').removeClass('error');
                $(cCell).removeAttr('data-tooltip');
            }
        }
    }
    var result = errors,
    j = result.length;
    result.sort();
    while (j--) {
        if (result[j] == result[j-1]) {
            result.splice(j, 1);
        }
    }
    return result;
}

function errorMessage(){
    $("[data-tooltip]").mousemove(function (eventObject) {
        $data_tooltip = $(this).attr("data-tooltip");
        $("#tooltip").text($data_tooltip)
                     .css({
                         "top" : eventObject.pageY + 5,
                        "left" : eventObject.pageX + 5
                     })
                     .show();
    }).mouseout(function () {
        $("#tooltip").text("")
                     .css({
                         "top" : 0,
                        "left" : 0
                     }).hide();
    });
}

/*forrmatter of collected data(to send to server)
*/
function sellerVals() {
    global_data.length=0;
    for(var i=1;i<sellers_global.length;i++){
        if(sellers_global[i]!=undefined){
            global_data.push(sellers_global[i]);
        } else{
            continue;
        }
    }
    sellers_global.length=0;
}
/*add new row
*/
function addRow(){
    $('#send').prop('disabled',true);
    var tbody=$('#seller-tbody');
    var rowId=tbody.children().length+1;
    var tr=$('<tr>');
    tr.attr('id',rowId);
    tr.attr('onchange',"sellerChange(this)");
    tbody.append(tr);

    for(var i=0;i<8;i++){
        var td=$('<td>');
        var input=$('<input>');
        input.attr('id','txt'+rowId+''+i);
        input.attr('type','text');
        input.attr('class','correct');
        input.attr('onfocus','currentCell(this)');
        td.append(input);
        tr.append(td);
    }
    $('#txt'+rowId+''+0).attr('readonly','true');
    $('#txt'+rowId+''+8).attr('readonly','true');
    $('#txt'+rowId+''+5).attr('type','email');
    $('#txt'+rowId+''+6).attr('type','tel');
}

/*function gets all tokens
*/
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
/*Store all tokens to csrftoken global variable
*/
var csrftoken = $.cookie('csrftoken');


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
};

$(document).ready(function(){
    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
});

/*Function sends ajax request which contains all changes and updates
*/
function Send(){
    if(checkErrors().length==0) {
        sellerVals();
        $.ajax({
            url: "/administrate/addorupdate/", // the endpoint
            type: "POST", // http method
            data: {"sellers": JSON.stringify(global_data)},
            success: function (response) {
                if (response == "OK") {
                    location.reload();
                } else {

                    console.log(JSON.stringify(global_data));
                }
            }

        });
        $('#send').prop('disabled',true);
    }
}
/*ajax request to delete seller
*/
function deleteAjax(){
    rowId=$('#txt'+rowNumber+0).val();
    if(rowId!=undefined) {
        $.ajax({
            url: "/administrate/deleteseller/",
            type:"POST",
            data: {"id": JSON.stringify(rowId)},
            success: function () {
                //here you do the processing needed to remove the row from the html;
            }
        });
    }
}
$(document).ready(function() {
    $('#seller-table').DataTable( {
        "pagingType": "full_numbers",
        "info":false,
        "lengthMenu": [ [10, 15, 20, -1], [10, 15, 20, "Բոլորը"] ],
        "searching": false,
        "language": {
            "lengthMenu": "Տեսնել _MENU_ տող",
            "paginate": {
                "first": "Սկիզբ",
                "previous": "Նախորդ",
                "next": "Հաջորդ",
                "last": "Վերջ"
            }
        }

    } );
} );

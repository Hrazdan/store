To create tables data from dump
  Open terminal and run

  mysql -u 'User name' -p'password' 'Database name' < /store/Database/Store.txt

To create dump file 
  Open terminal and run

  mysqldump -u 'User name' -p'password' --no-create-info 'Database name' 'Table names(separated with spaces)' > /store/Database/Store.txt